package com.expeditors.training.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.expeditors.training.data.Product;

public class ProductService {
	static final String JDBC_DRIVER = "org.apache.derby.jdbc.ClientDriver";
	static final String DB_URL = "jdbc:derby://localhost:1527/Users/apgrnt/glebe/derbydata/productsales";
	static final String USER = "base";
	static final String PASS = "base";

	public List<Product> getProducts() throws SQLException,
			ClassNotFoundException {
		Connection conn = null;
		Statement stmt = null;
		try {
			// Register JDBC driver
			Class.forName(JDBC_DRIVER);
			// Open a Connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			// Now you can e extract all the records
			String sql = "SELECT name, description, price FROM Product";
			ResultSet rs = stmt.executeQuery(sql);

			List<Product> products = new ArrayList<Product>();
			while (rs.next()) {
				// Retrieve by column name
				String name = rs.getString("name");
				String description = rs.getString("description");
				Double price = rs.getDouble("price");
				// add to prodcuts
				products.add(new Product(name, description, price));
			}
			rs.close();
			return products;
		} finally {
			// finally block used to close resources
			if (stmt != null)
				stmt.close();
			if (conn != null)
				conn.close();
		}
	}

	public void addProduct(Product product) throws SQLException,
			ClassNotFoundException {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// Register JDBS driver
			Class.forName(JDBC_DRIVER);

			// open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// setup sql
			String sql = "INSERT INTO PRODUCT(NAME, DESCRIPTION, PRICE) VALUES(?, ?, ?)";
			stmt = conn.prepareStatement(sql);

			// set parameters
			stmt.setString(1, product.getName());
			stmt.setString(2, product.getDescription());
			stmt.setDouble(3, product.getPrice());

			stmt.executeUpdate();
		} finally {
			if (stmt != null)
				stmt.close();
			if (conn != null)
				conn.close();
		}
	}

	public void removeProduct(String name) throws SQLException,
			ClassNotFoundException {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// Register JDBS driver
			Class.forName(JDBC_DRIVER);

			// open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// setup sql
			String sql = "DELETE FROM PRODUCT WHERE NAME IN (?) ";
			stmt = conn.prepareStatement(sql);

			// set parameters
			stmt.setString(1, name);

			stmt.executeUpdate();
		} finally {
			if (stmt != null)
				stmt.close();
			if (conn != null)
				conn.close();
		}
	}

	public Product getProductByName(String name) throws SQLException,
			ClassNotFoundException {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// Register JDBS driver
			Class.forName(JDBC_DRIVER);

			// open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// setup sql
			String sql = "SELECT * FROM PRODUCT WHERE NAME = ?";
			stmt = conn.prepareStatement(sql);

			// set parameters
			stmt.setString(1, name);

			ResultSet rs = stmt.executeQuery();
			rs.next();
			String name2 = rs.getString("name");
			String description = rs.getString("description");
			double price = rs.getDouble("price");
			int productID = rs.getInt("product_id");

			Product toReturn = new Product(name2, description, price, productID);
			rs.close();
			return toReturn;
		} finally {
			if (stmt != null)
				stmt.close();
			if (conn != null)
				conn.close();
		}
	}

	public void addOrder(String name, String email) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// Register JDBS driver
			Class.forName(JDBC_DRIVER);

			// open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			
			String sql = "SELECT PRODUCT_ID FROM PRODUCT WHERE NAME = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			int productID = rs.getInt("product_id");

			sql = "INSERT INTO ORDERS(PRODUCT_ID, EMAIL) VALUES(?,?)";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, productID);
			stmt.setString(2, email);
			
			stmt.executeUpdate();
		} finally {
			if (stmt != null)
				stmt.close();
			if (conn != null)
				conn.close();
		}

	}

	public List<Order> getOrders() throws SQLException, ClassNotFoundException {
		Connection conn = null;
		Statement stmt = null;
		try {
			// Register JDBC driver
			Class.forName(JDBC_DRIVER);
			// Open a Connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			// Now you can e extract all the records
			String sql = "SELECT ORDERS.EMAIL, PRODUCT.NAME FROM PRODUCT JOIN ORDERS ON ORDERS.PRODUCT_id=PRODUCT.PRODUCT_id ORDER BY EMAIL";
			ResultSet rs = stmt.executeQuery(sql);

			List<Order> orders = new ArrayList<Order>();
			while (rs.next()) {
				// Retrieve by column name
				String email = rs.getString("email");
				String name = rs.getString("name");
				orders.add(new Order(email, name));
			}
			rs.close();
			return orders;
		} finally {
			// finally block used to close resources
			if (stmt != null)
				stmt.close();
			if (conn != null)
				conn.close();
		}
	}

}
