package com.expeditors.training.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ContactServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    protected void doGet(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/contact.jsp");
		dispatcher.forward(request, response);
    }


    protected void doPost(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException {
	String email = request.getParameter("email");
	String message = request.getParameter("message");

	if (message.length() == 0 || message.trim().length()<0) {
	    request.setAttribute("errorMessage",  "Please input a message.");
	    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/contact.jsp");
	    dispatcher.forward(request, response);
	    //response.sendRedirect("contact.jsp?email="+email);
	} else {
	    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/confirm.jsp");
		dispatcher.forward(request, response);
	}
    }

}
