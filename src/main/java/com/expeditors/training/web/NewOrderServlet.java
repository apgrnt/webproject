package com.expeditors.training.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.expeditors.training.data.Product;
import com.expeditors.training.service.ProductService;

public class NewOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 ProductService productService = new ProductService();
       
    public NewOrderServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Object name = request.getSession().getAttribute("name");
		try {
			Product product = productService.getProductByName(name.toString());
			request.setAttribute("product", product);
			getServletContext().getRequestDispatcher("/WEB-INF/confirmOrder.jsp").forward(request, response);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getServletContext().getRequestDispatcher("/WEB-INF/confirmOrder.jsp").forward(request, response);
			String name = request.getParameter("name");
			String email = request.getParameter("email");
			try {
				productService.addOrder(name, email);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			response.sendRedirect(request.getContextPath() + "/orderConfirmation");
	}
}
