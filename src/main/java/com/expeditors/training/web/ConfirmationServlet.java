package com.expeditors.training.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.expeditors.training.data.Product;
import com.expeditors.training.service.Order;
import com.expeditors.training.service.ProductService;

public class ConfirmationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 ProductService productService = new ProductService();
       
    public ConfirmationServlet() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Object name = request.getSession().getAttribute("name");
		List<Order> orders = null;
		try {
			orders = productService.getOrders();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		request.setAttribute("orders", orders);
		getServletContext().getRequestDispatcher("/WEB-INF/showOrders.jsp").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
