package com.expeditors.training.web;

import java.util.List;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.expeditors.training.data.Product;
import com.expeditors.training.service.ProductService;

public class RemoveProductServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    ProductService productService = new ProductService();

    public RemoveProductServlet() {
	super();
    }

    protected void doGet(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException {
	List<Product> products = null;
	try {
	    products = productService.getProducts();
	} catch (ClassNotFoundException | SQLException e) {
	    e.printStackTrace();
	}
	request.setAttribute("products", products);
	getServletContext().getRequestDispatcher("/WEB-INF/removeProduct.jsp")
		.forward(request, response);
    }

    protected void doPost(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException {
	String name = request.getParameter("name");
	try {
	    productService.removeProduct(name);
	} catch (ClassNotFoundException | SQLException e) {
	    e.printStackTrace();
	}
	response.sendRedirect(request.getContextPath() + "/removeProducts");
    }

}
