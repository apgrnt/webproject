package com.expeditors.training.web;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.expeditors.training.data.Product;
import com.expeditors.training.service.ProductService;

public class AddProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 ProductService productService = new ProductService();
       
    public AddProductServlet() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getServletContext().getRequestDispatcher("/WEB-INF/addProduct.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String desc = request.getParameter("description");
		String pricestr = request.getParameter("price");
		double price = 0.0;
		if(pricestr != null && pricestr.length()>0) {
		    price = Double.valueOf(pricestr);
		}
		try {
		    productService.addProduct(new Product(name, desc, price));
		} catch (ClassNotFoundException | SQLException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
		response.sendRedirect(request.getContextPath() + "/products");
	}

}
