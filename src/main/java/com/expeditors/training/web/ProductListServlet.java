package com.expeditors.training.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.expeditors.training.data.Product;
import com.expeditors.training.service.ProductService;

public class ProductListServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    ProductService productService = new ProductService();
    
    public ProductListServlet() {
	super();
    }

    protected void doGet(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException {
	List<Product> products = null;
	try {
	    products = productService.getProducts();
	} catch (ClassNotFoundException e) {
	    e.printStackTrace();
	} catch (SQLException e) {
	    e.printStackTrace();
	}
	
	request.setAttribute("products", products);
	getServletContext().getRequestDispatcher("/WEB-INF/products.jsp").forward(request, response);
    }
    

    protected void doPost(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException {
	String name = request.getParameter("name");
	
	request.getSession().setAttribute("name", name);
	
	response.sendRedirect(request.getContextPath() + "/neworder");
    }

}
