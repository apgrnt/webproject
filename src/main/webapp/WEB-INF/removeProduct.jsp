<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<title>Remove a Product by Name</title>
</head>
<body>
	<h1>Remove a product from the list</h1>
	<form method="POST" action="removeProducts">
		Name: <input id="name" type="text" name="name" /><br /> 
		<input id="submit" type="submit" value="Submit" />
	</form>

	<c:forEach items="${products}" var="product">
		<h2>${product.name}</h2>
		<p>${product.description}
			<fmt:formatNumber value="${product.price}" type="currency" />
		</p>
		<form method="POST" action="removeProducts">
			<button class="btn" id="RemoveProduct" name="name" type="submit" value="${product.name }">Remove Product</button>
		</form>
	</c:forEach>
</body>
</html>


