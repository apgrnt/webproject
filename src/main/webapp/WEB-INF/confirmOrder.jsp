<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
	<head>
	<title>Confirm Order</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	</head>
<body>	
		<div class="row text-center">
			<h2>Enter Email to Confirm order</h2>
			<h3>${product.name }</h3>
			${product.description } <br/>
			${product.id }<br/>
			<fmt:formatNumber value="${product.price}" type="currency"/> <br/>
			<form method="POST" action="neworder">
			Email: <input id="email" type="email" name="email" /><br/>
			<button class="btn" name="name" id="Order" type="submit" value="${product.name }">confirm</button>
			</form>
		</div>
</body>
</html>