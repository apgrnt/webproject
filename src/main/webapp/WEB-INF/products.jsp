<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
	<head>
		<title>Products for sale</title>
	</head>
	<body>
		<c:forEach items="${products}" var="product">
			<h2>${product.name}</h2>
			<p>${product.description} <br/>
			<fmt:formatNumber value="${product.price}" type="currency"/>
			</p>
			<form method="POST" action="products">
				<button class="btn" name="name" id="Order" type="submit" value="${product.name }">Order</button>
			</form>
		</c:forEach>
	</body>
</html>