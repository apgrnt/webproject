<?xml version="1.0" encoding="UTF-8" ?>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<title></title>
</head>

<body>
	<h1><decorator:title/></h1>
	
	<p><b></b></p>
	
	<%@ include file = "/navbar.html" %>
	
	<hr />
	<p></p>
	<decorator:body/>
	<hr />
	<h1>©Copyright 2015 Expeditors Training</b></h1>
	
</body>
</html>