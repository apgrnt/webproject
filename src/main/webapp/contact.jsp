<html>
<head>

<title>Contact Me</title>

<script> $( document ).ready(function() {
	$('#submit').click(function(e){
		if($('#email').val() == '') {
			e.preventDefault();
			$('#email').css('background-color', 'red');
		}
	});
});</script>

</head>
<body>
	<div class="row text-center">
		<h1>Call me Anytime</h1>
		<form method="POST" action="contact">
			email: <input id="email" type="text" name="email" value='${param.email}' /><br /> 
			message: <textarea id="message" cols="40" rows="5" name="message"></textarea><br />
			<p class="error">${errorMessage}</p>
			<input class="btn btn-danger" id="submit" type="submit" value="submit" />
		</form>
	</div>
	
</body>

</html>